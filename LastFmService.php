<?php

namespace App/Services;

use App\Models\Album;
use App\Models\Artist;
use App\Models\Song;
use App\Models\User;
use App\Services\LastFmApiClient;
use App\Values\AlbumInformation;
use App\Values\ArtistInformation;

use GuzzleHttp\Promise\Promise;
use GuzzleHttp\Promise\Utils;
use Illuminate\Support\Collection;


class LastFmService implements ThirdPartyMusicServiceInterface
{
    const METHOD_ARTIST_INFO = 'artist.getInfo';

    const METHOD_ALBUM_INFO = 'album.getInfo';

    private LastFmApiClient $client;


    public function __construct(LastFmApiClient $client)
    {
        $this->client = $client;
    }


    public static function used(): bool
    {
        return (bool) config('radio.lastfm.key');
    }


    public static function enabled(): bool
    {
        return config('radio.lastfm.key') && config('radio.lastfm.secret');
    }


    private function buildApiEndpoint(string $method, array $params): string
    {
        $params['format'] = 'json';
        $params['autocorrect'] = 1;
   
        return '?' . http_build_query(['method' => $method] + $params);
    }

   
    private function isLastfmEnabled(): bool
    {
        return static::enabled();
    }

   
    public function getArtistInformation(Artist $artist): ?ArtistInformation
    {
        return attempt_if($this->isLastfmEnabled(), function () use ($artist): ?ArtistInformation {
            $name = urlencode($artist->name);
            $response = $this->client->get($this->buildApiEndpoint(self::METHOD_ARTIST_INFO, compact('name')));

            return isset($response->artist)
                ? ArtistInformation::fromLastFmData($response->artist)
                : null;
        });
    }


    public function getAlbumInformation(Album $album): ?AlbumInformation
    {
        return attempt_if($this->isLastfmEnabled(), function () use ($album): ?AlbumInformation {
            
            $albumName = urlencode($album->name);
            $artistName = urlencode($album->artist->name);
            $response = $this->client
                ->get($this->buildApiEndpoint(
                    self::METHOD_ALBUM_INFO, compact('albumName', 'artistName')
                ));

            return isset($response->album)
                ? AlbumInformation::fromLastFmData($response->album)
                : null;
        });
    }


    public function scrobbleTrack(Song $song, User $user, int $timestamp): void
    {
        $params = [
            'artist' => $song->artist->name,
            'track' => $song->title,
            'timestamp' => $timestamp,
            'sk' => $user->lastfm_session_key,
            'method' => 'track.scrobble',
        ];

        if ($song->album->name !== Album::UNKNOWN_NAME) {
            $params['album'] = $song->album->name;
        }

        attempt(fn () => $this->client->post('/', $params, false));
    }


    public function toggleLoveTrack(Song $song, User $user, bool $love): void
    {
        attempt(fn () => $this->client->post('/', [
            'track' => $song->title,
            'artist' => $song->artist->name,
            'sk' => $user->lastfm_session_key,
            'method' => $love ? 'track.love' : 'track.unlove',
        ], false));
    }


    /**
     * @param Collection|array<array-key, Song> $songs
     */
    public function batchToggleLoveTracks(Collection $songs, User $user, bool $love): void
    {
        $promises = $songs->map(
            function (Song $song) use ($user, $love): Promise {
                return $this->client->postAsync('/', [
                    'track' => $song->title,
                    'artist' => $song->artist->name,
                    'sk' => $user->lastfm_session_key,
                    'method' => $love ? 'track.love' : 'track.unlove',
                ], false);
            }
        );

        attempt(static fn () => Utils::unwrap($promises));
    }


    public function updateNowPlaying(Song $song, User $user): void
    {
        $params = [
            'artist' => $song->artist->name,
            'track' => $song->title,
            'duration' => $song->length,
            'sk' => $user->lastfm_session_key,
            'method' => 'track.updateNowPlaying',
        ];

        if ($song->album->name !== Album::UNKNOWN_NAME) {
            $params['album'] = $song->album->name;
        }

        attempt(fn () => $this->client->post('/', $params, false));

    }


    public function getSessionKey(string $token): ?string
    {
        return $this->client->getSessionKey($token);
    }


    public function setUserSessionKey(User $user, ?string $sessionKey): void
    {
        $user->preferences->lastFmSessionKey = $sessionKey;
        $user->save();
    }
}